import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CounterComponent } from './counter.component';
import { CounterService } from './counter.service';

@NgModule({
  declarations: [CounterComponent],
  exports: [CounterComponent],
  providers: [],
  imports: [
    CommonModule
  ]
})
export class CounterModule {
  static forRoot(val: number = 0): ModuleWithProviders<CounterModule> {
    return {
      ngModule: CounterModule,
      providers: [
        {
          provide: CounterService,
          useFactory: () => {
            return new CounterService(val);
          }
        }
      ]
    };
  }
}
