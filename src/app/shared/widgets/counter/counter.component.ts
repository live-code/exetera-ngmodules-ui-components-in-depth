import { Component, OnInit } from '@angular/core';
import { CounterService } from './counter.service';

@Component({
  selector: 'app-counter',
  template: `
    <div>
      Counter {{counterService.value}}
    </div>
  `,
  styles: [
  ]
})
export class CounterComponent {

  constructor(public counterService: CounterService) {
    counterService.inc();
  }

}
