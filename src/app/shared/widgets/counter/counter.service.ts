import { Injectable } from '@angular/core';

@Injectable()
export class CounterService {
  private val = 0;

  constructor(private config: number) {
    this.val = config;
  }
  inc(): void {
    this.val++;
  }
  get value(): number {
    return this.val;
  }
}
