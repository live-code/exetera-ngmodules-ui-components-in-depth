import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { COMPONENTS } from './components';
import { GooglemapComponent } from './components/googlemap.component';
import { AccordionComponent } from './components/accordion.component';
import { AccordionGroupComponent } from './components/accordion-group.component';

@NgModule({
  declarations: [...COMPONENTS, AccordionComponent, AccordionGroupComponent],
  exports: [...COMPONENTS, AccordionComponent, AccordionGroupComponent],
  imports: [
    CommonModule
  ]
})
export class UikitModule { }
