import { Component, Input, OnInit } from '@angular/core';

export enum Sizes {
  xs = 50,
  sm = 100,
  md = 150,
  xl = 220,
}

enum Backgrounds {
  primary = '#F1F1F1',
  highlights = '#615375',
  warning = '#E5625C',
}
enum Texts {
  primary = '#222',
  highlights = 'white',
  warning = 'white',
}

/**
 * SImple box with text and image
 */
@Component({
  selector: 'app-box',
  template: `

      <div 
        class="box"
        style="display: flex; justify-content: center; align-items: center;"
        [style.justify-content]="alignCenter ? 'center' : null"
        [style.height.px]="SIZES[size]"
        [style.background-color]="BACKGROUNDS[bg]"
        [style.color]="TEXTS[bg]"
        [style.padding.px]="padding ? 30 : 0 "
      >
        <img src="assets/logo.png" alt="" *ngIf="showLogo" style="padding: 1rem">
        <ng-content></ng-content>
      </div>

  `,
  styles: [`
    .box {
      /*background-color: #f1f1f1;*/
      border-radius: 20px;
      /*padding: 20px;*/
      color: #777;
      margin-top: 15px;
    }
  `]
})
export class BoxComponent {
  /**
   * if apply pad or not
   */
  @Input() padding = true;
  /**
   * if logo should be displayed
  */
  @Input() showLogo = false;
  @Input() alignCenter = true;
  @Input() bg: 'primary' | 'highlights' | 'warning' = 'primary';
  @Input() size: 'xs' | 'sm' | 'md' | 'xl' | null = 'xs';
  SIZES = Sizes;
  BACKGROUNDS = Backgrounds;
  TEXTS = Texts;


}
