import { BoxComponent } from './box.component';
import { GridComponent } from './grid.component';
import { ColComponent } from './col.component';
import { SeparatorComponent } from './separator.component';
import { AutoGridComponent } from './auto-grid.component';
import { ChartjsComponent } from './chartjs.component';
import { SuperAutoGridComponent } from './super-auto-grid.component';
import { GooglemapComponent } from './googlemap.component';

export const COMPONENTS = [
  BoxComponent,
  GridComponent,
  ColComponent,
  SeparatorComponent,
  AutoGridComponent,
  ChartjsComponent,
  GooglemapComponent,
  SuperAutoGridComponent
];
