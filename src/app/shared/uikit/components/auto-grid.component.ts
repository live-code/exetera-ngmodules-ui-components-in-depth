import { AfterViewInit, Component, ElementRef, HostBinding, OnInit, Renderer2, ViewChild, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-auto-grid',
  // encapsulation: ViewEncapsulation.None,
  template: `
      <ng-content></ng-content>
  `,
  styles: [`
    :host {
      display: flex;
      flex-direction: row;
    }
   /* :host .cell {
      flex-grow: 1;
    }*/
  `]
})
export class AutoGridComponent implements AfterViewInit {

  constructor(private renderer2: Renderer2, private el: ElementRef<any>) { }

  ngAfterViewInit(): void {
    for (const item of this.el.nativeElement.children) {
      this.renderer2.setStyle(item, 'flex-grow', 1);
    }
    if (this.el.nativeElement.children.length === 0) {
      throw new Error('<auto-grid> non può essere usato senza figli')
    }
  }
}
