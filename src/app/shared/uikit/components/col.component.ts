import { Component, HostBinding, OnInit } from '@angular/core';

@Component({
  selector: 'app-col',
  template: `
      <ng-content></ng-content>
  `,
  styles: [`
    :host.column {
      flex-grow: 1;
      margin-right: 20px;
    }
    :host.column:last-child {
      margin-right: 0;
    }
  `]
})
export class ColComponent {
  @HostBinding() class = 'column';
}
