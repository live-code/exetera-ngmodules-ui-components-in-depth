import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import * as Chart from 'chart.js';
import { updateChart } from './chartjs.utils';

@Component({
  selector: 'app-chartjs',
  template: `
    <div style="width: 100%">
      <canvas #host></canvas>
    </div>
  `,
})
export class ChartjsComponent implements OnChanges  {
  @ViewChild('host', { static: true }) host: ElementRef<HTMLCanvasElement>;
  @Input() config: number[];
  chart: Chart;

  draw(cfg): void {
    const config = updateChart(cfg);
    this.chart = new Chart(this.host.nativeElement.getContext('2d'), config);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(this.config)

    const cfg = updateChart(this.config);

    if (changes.config.isFirstChange()) {
      this.chart = new Chart(this.host.nativeElement.getContext('2d'), cfg);
    } else {
      this.chart.config = cfg;
      this.chart.update();
    }
  }

}
