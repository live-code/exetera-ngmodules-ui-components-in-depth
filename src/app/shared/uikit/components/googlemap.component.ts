import { AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChange, SimpleChanges, ViewChild } from '@angular/core';

@Component({
  selector: 'app-googlemap',
  template: `
    <div #host style="width: 100%" [style.height.px]="height"></div>
  `,
})
export class GooglemapComponent implements OnChanges {
  @ViewChild('host', { static: true }) host: ElementRef<HTMLDivElement>;
  @Input() lat: number;
  @Input() lng: number;
  @Input() zoom: number;
  @Input() height = 300;
  map: google.maps.Map;
  marker: google.maps.Marker;
/*
  ngAfterViewInit(): void {
    this.map = new google.maps.Map(this.host.nativeElement, {
      center: { lat: -34.397, lng: 150.644 },
      zoom: 8,
    });
  }*/

  ngOnChanges(changes: SimpleChanges): void {
    const { lat, lng, zoom } = changes;
    this.render(lat, lng, zoom)
  }

  render(lat: SimpleChange, lng: SimpleChange, zoom: SimpleChange): void {
    if (!this.map) {
      const opts = { zoom: zoom.currentValue || 5 };
      this.map = new google.maps.Map(this.host.nativeElement, opts);
    }

    if (lat && lng) {
      const latLng = new google.maps.LatLng(lat.currentValue, lng.currentValue);
      this.map.panTo(latLng);

      if (this.marker) {
        this.marker.setMap(null)
      }
      this.marker  = new google.maps.Marker({
        position: latLng,
        map: this.map,
        title: 'Hello World!',
      });
    }

    if (zoom) {
      this.map.setZoom(zoom.currentValue)
    }
  }
}
