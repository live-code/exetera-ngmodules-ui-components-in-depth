import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-accordion-group',
  animations: [
    trigger('collapsable', [
      state('opened', style({
        height: '*'
      })),
      state('closed', style({
        height: '0px',
        padding: 0
      })),
      transition('opened => closed', [
        animate('0.4s cubic-bezier(0.77, 0, 0.175, 1)')
      ]),
      transition('closed => opened', [
        animate('1.4s cubic-bezier(0.77, 0, 0.175, 1)')
      ])
    ])
  ],
  template: `
    <div class="mypanel">
      <div class="title" (click)="toggle.emit()">{{title}}</div>
      <div class="body" [@collapsable]="opened ? 'opened' : 'closed'">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [`
    .mypanel {
      background-color: white;
      color: #222;
    }

    .mypanel .title {
      background-color: #e2e2e2;
      width: 100%;
      padding: 10px;
      cursor: pointer;
      border-bottom: 1px solid #999;
    }

    .mypanel .body {
      padding: 20px;
      overflow: hidden;
    }
  `]
})
export class AccordionGroupComponent {
  @Input() title: string;
  @Input() opened: boolean;
  @Output() toggle: EventEmitter<void> = new EventEmitter<void>();
}
