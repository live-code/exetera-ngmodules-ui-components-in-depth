import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  DoCheck,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChildren
} from '@angular/core';
import { AccordionGroupComponent } from './accordion-group.component';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-accordion',
  template: `
    <ng-content></ng-content>
  `,
})
export class AccordionComponent implements AfterViewInit, OnDestroy, DoCheck {
  @ContentChildren(AccordionGroupComponent) groups: QueryList<AccordionGroupComponent>;
  prevGroups: QueryList<AccordionGroupComponent>;
  destroy$: Subject<void> = new Subject<void>();

  constructor(private cd: ChangeDetectorRef) { }

  ngAfterViewInit(): void {
    this.groups.toArray().forEach(g => {
      g.toggle
        .pipe(
          takeUntil(this.destroy$)
        )
        .subscribe(() => {
        console.log('clicked', g)
        this.openGroup(g);
      });
    });
  }

  ngDoCheck(): void {
    if (this.prevGroups !== this.groups) {
      this.groups.toArray()[0].opened = true;
      this.prevGroups = this.groups;
    }
  }

  openGroup(g: AccordionGroupComponent): void {
    this.groups.toArray().forEach(g => g.opened = false);
    g.opened = true;
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
