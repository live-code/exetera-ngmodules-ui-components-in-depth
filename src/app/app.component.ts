import { Component } from '@angular/core';
import { CounterService } from './shared/widgets/counter/counter.service';

@Component({
  selector: 'app-root',
  template: `
    <app-navbar></app-navbar>
    <router-outlet></router-outlet>
    
  `,
})
export class AppComponent {
  title = 'angular-demo-ui';

  constructor(private counter: CounterService) {
    // counter.value = 10;
  }
}
