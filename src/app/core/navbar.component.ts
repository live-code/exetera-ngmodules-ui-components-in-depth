import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  template: `
    <button routerLink="home">home</button>
    <button routerLink="catalog">catalog</button>
    <button routerLink="contacts">contacts</button>
    <hr>

  `,
  styles: [
  ]
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
