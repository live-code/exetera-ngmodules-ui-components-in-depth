import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contacts',
  template: `
    <app-box [showLogo]="true" [alignCenter]="false" size="sm">CONTATTO</app-box>
    <app-counter></app-counter>
  `,
  styles: [
  ]
})
export class ContactsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
