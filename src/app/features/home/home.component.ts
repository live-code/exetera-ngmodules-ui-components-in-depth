import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { AccordionGroupComponent } from '../../shared/uikit/components/accordion-group.component';

@Component({
  selector: 'app-home',
  template: `
    <app-accordion>
      <app-accordion-group [title]="user.title" *ngFor="let user of users">
        bla bla
      </app-accordion-group>
      <app-accordion-group title="1">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad atque autem consequatur cumque, cupiditate doloribus dolorum esse et eveniet expedita facere, magni molestiae optio sit totam vel, voluptate voluptates.
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad atque autem consequatur cumque, cupiditate doloribus dolorum esse et eveniet expedita facere, magni molestiae optio sit totam vel, voluptate voluptates.
      </app-accordion-group>
      <app-accordion-group title="2">
        bla bla
      </app-accordion-group>
    </app-accordion>
    
    <div style="width: 100%; max-width: 500px; margin: 0 auto">
      <app-googlemap [height]="200" [lat]="lat" [lng]="lng" [zoom]="zoom"></app-googlemap>
      <button (click)="lat = 30; lng = 20">1</button>
      <button (click)="lat = 40; lng = 10">2</button>
      <button (click)="zoom = zoom + 1">+</button>
      <button (click)="zoom = zoom - 1">-</button>
      <app-home-carousel></app-home-carousel>
      
      <app-separator [margin]="30" type="dashed" color="red"></app-separator>
      <app-home-news></app-home-news>
      <app-separator [margin]="30" type="dashed" color="red"></app-separator>
      <app-counter></app-counter>
    </div>
  `,
})
export class HomeComponent {
  lat = 43;
  lng = 13;
  zoom = 5;

  users = [
    { id: 1, title: 'fabio', opened: true },
    { id: 2, title: 'mario', opened: false }
  ];


}
