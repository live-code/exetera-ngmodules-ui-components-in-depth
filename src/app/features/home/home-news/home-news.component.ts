import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-news',
  template: `
    <app-grid>
      <app-col>
        <app-box size="md">1 </app-box>
      </app-col>
      <app-col>
        <app-box size="md"> 2</app-box>
      </app-col>
      <app-col>
        <app-box size="md">3 </app-box>
      </app-col>
      <app-col>
        <app-box size="md">4</app-box>
      </app-col>
    </app-grid>
  `,
  styles: [
  ]
})
export class HomeNewsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
