import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-carousel',
  template: `

    <app-super-auto-grid>
      <div>1</div>
      <div>2</div>
      <div>3</div>
    </app-super-auto-grid>
    
   <app-chartjs [config]="data"></app-chartjs>
   <button (click)="data = [1, 22, 33, 1, 33, 66]">update1  data</button>
   <button (click)="data = [31, 52, 44, 12, 45, 22]">update2  data</button>
   <app-auto-grid>
     <div class="btn btn-outline-primary">1</div>
     <div>2</div>
     <div>3</div>
   </app-auto-grid>
   
    <app-box bg="highlights">Lorem ipsum dolor</app-box>
    <app-box [showLogo]="true" size="xl"> </app-box>

    <app-grid>
      <app-col>
        <app-box [showLogo]="true" size="md" > </app-box>
      </app-col>
      <app-col>
        <app-box [showLogo]="true" size="md"> </app-box>
      </app-col>
    </app-grid>
  `,
  styles: [
  ]
})
export class HomeCarouselComponent implements OnInit {
  data = [50, 100, 50, 100, 50, 100];

  constructor() { }

  ngOnInit(): void {
  }

}
