import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeCarouselComponent } from './home-carousel.component';
import { UikitModule } from '../../../shared/uikit/uikit.module';



@NgModule({
  declarations: [HomeCarouselComponent],
  exports: [HomeCarouselComponent],
  imports: [
    CommonModule,
    UikitModule
  ]
})
export class HomeCarouselModule { }
