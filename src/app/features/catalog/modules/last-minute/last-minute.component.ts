import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-last-minute',
  template: `
    <p>
      last-minute works!
    </p>
  `,
  styles: [
  ]
})
export class LastMinuteComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
