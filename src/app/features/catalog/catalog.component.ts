import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-catalog',
  template: `
    <div>
      <button routerLinkActive="bg-dark text-white" routerLink="help">go to help</button>
      <button routerLinkActive="bg-dark text-white" routerLink="products">products</button>
      <button routerLinkActive="bg-dark text-white" routerLink="/catalog/offers">offers</button>
      <button routerLinkActive="bg-dark text-white" routerLink="last-minute">last</button>
    
      <hr>  
      <router-outlet></router-outlet>
    </div>
  `,
  styles: [
  ]
})
export class CatalogComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
