import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { UikitModule } from '../../shared/uikit/uikit.module';
import { CatalogHelpComponent } from './catalog-help.component';


@NgModule({
  declarations: [CatalogComponent, CatalogHelpComponent],
  imports: [
    CommonModule,
    CatalogRoutingModule,
    UikitModule
  ]
})
export class CatalogModule { }
