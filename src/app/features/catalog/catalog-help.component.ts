import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-catalog-help',
  template: `

    <hr>
    <button routerLink="../">back</button>
    <p>
      catalog-help works!
    </p>
  `,
  styles: [
  ]
})
export class CatalogHelpComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
