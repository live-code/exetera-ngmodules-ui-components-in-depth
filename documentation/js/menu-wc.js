'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">angular-demo-ui documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-e98e8f55253f956f262c386bddff64f9"' : 'data-target="#xs-components-links-module-AppModule-e98e8f55253f956f262c386bddff64f9"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-e98e8f55253f956f262c386bddff64f9"' :
                                            'id="xs-components-links-module-AppModule-e98e8f55253f956f262c386bddff64f9"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NavbarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NavbarComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CatalogModule.html" data-type="entity-link">CatalogModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CatalogModule-af9a3131cf13686e0fc04bb3702bd595"' : 'data-target="#xs-components-links-module-CatalogModule-af9a3131cf13686e0fc04bb3702bd595"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CatalogModule-af9a3131cf13686e0fc04bb3702bd595"' :
                                            'id="xs-components-links-module-CatalogModule-af9a3131cf13686e0fc04bb3702bd595"' }>
                                            <li class="link">
                                                <a href="components/CatalogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CatalogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CatalogHelpComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CatalogHelpComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/CatalogRoutingModule.html" data-type="entity-link">CatalogRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ContactsModule.html" data-type="entity-link">ContactsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ContactsModule-f452d4a4accc0a5ee3d6f7f689f78145"' : 'data-target="#xs-components-links-module-ContactsModule-f452d4a4accc0a5ee3d6f7f689f78145"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ContactsModule-f452d4a4accc0a5ee3d6f7f689f78145"' :
                                            'id="xs-components-links-module-ContactsModule-f452d4a4accc0a5ee3d6f7f689f78145"' }>
                                            <li class="link">
                                                <a href="components/ContactsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ContactsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ContactsRoutingModule.html" data-type="entity-link">ContactsRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CounterModule.html" data-type="entity-link">CounterModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-CounterModule-4e0cc87034d632a814b8310cced39ce6"' : 'data-target="#xs-components-links-module-CounterModule-4e0cc87034d632a814b8310cced39ce6"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-CounterModule-4e0cc87034d632a814b8310cced39ce6"' :
                                            'id="xs-components-links-module-CounterModule-4e0cc87034d632a814b8310cced39ce6"' }>
                                            <li class="link">
                                                <a href="components/CounterComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CounterComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomeCarouselModule.html" data-type="entity-link">HomeCarouselModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HomeCarouselModule-6a0d4078e17272dea3a074c9fe5e7430"' : 'data-target="#xs-components-links-module-HomeCarouselModule-6a0d4078e17272dea3a074c9fe5e7430"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HomeCarouselModule-6a0d4078e17272dea3a074c9fe5e7430"' :
                                            'id="xs-components-links-module-HomeCarouselModule-6a0d4078e17272dea3a074c9fe5e7430"' }>
                                            <li class="link">
                                                <a href="components/HomeCarouselComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HomeCarouselComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomeModule.html" data-type="entity-link">HomeModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HomeModule-b03b47d8eb3e7e475a8225853def71c5"' : 'data-target="#xs-components-links-module-HomeModule-b03b47d8eb3e7e475a8225853def71c5"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HomeModule-b03b47d8eb3e7e475a8225853def71c5"' :
                                            'id="xs-components-links-module-HomeModule-b03b47d8eb3e7e475a8225853def71c5"' }>
                                            <li class="link">
                                                <a href="components/HomeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HomeComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomeNewsModule.html" data-type="entity-link">HomeNewsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-HomeNewsModule-56b28b5306b2f72e989f1291e470c86b"' : 'data-target="#xs-components-links-module-HomeNewsModule-56b28b5306b2f72e989f1291e470c86b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-HomeNewsModule-56b28b5306b2f72e989f1291e470c86b"' :
                                            'id="xs-components-links-module-HomeNewsModule-56b28b5306b2f72e989f1291e470c86b"' }>
                                            <li class="link">
                                                <a href="components/HomeNewsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HomeNewsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/HomeRoutingModule.html" data-type="entity-link">HomeRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/LastMinuteModule.html" data-type="entity-link">LastMinuteModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LastMinuteModule-648b5a963d6e95f2b0044932f6fd7f34"' : 'data-target="#xs-components-links-module-LastMinuteModule-648b5a963d6e95f2b0044932f6fd7f34"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LastMinuteModule-648b5a963d6e95f2b0044932f6fd7f34"' :
                                            'id="xs-components-links-module-LastMinuteModule-648b5a963d6e95f2b0044932f6fd7f34"' }>
                                            <li class="link">
                                                <a href="components/LastMinuteComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LastMinuteComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LastMinuteRoutingModule.html" data-type="entity-link">LastMinuteRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/OffersModule.html" data-type="entity-link">OffersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-OffersModule-ca500d453a19e6f77f7cf9e797b914d7"' : 'data-target="#xs-components-links-module-OffersModule-ca500d453a19e6f77f7cf9e797b914d7"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-OffersModule-ca500d453a19e6f77f7cf9e797b914d7"' :
                                            'id="xs-components-links-module-OffersModule-ca500d453a19e6f77f7cf9e797b914d7"' }>
                                            <li class="link">
                                                <a href="components/OffersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">OffersComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/OffersRoutingModule.html" data-type="entity-link">OffersRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ProductsModule.html" data-type="entity-link">ProductsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ProductsModule-32898077325ae80a431abd8549629481"' : 'data-target="#xs-components-links-module-ProductsModule-32898077325ae80a431abd8549629481"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ProductsModule-32898077325ae80a431abd8549629481"' :
                                            'id="xs-components-links-module-ProductsModule-32898077325ae80a431abd8549629481"' }>
                                            <li class="link">
                                                <a href="components/ProductsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ProductsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/ProductsRoutingModule.html" data-type="entity-link">ProductsRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/UikitModule.html" data-type="entity-link">UikitModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UikitModule-154f798c77e8b15a35b3161d22cae498"' : 'data-target="#xs-components-links-module-UikitModule-154f798c77e8b15a35b3161d22cae498"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UikitModule-154f798c77e8b15a35b3161d22cae498"' :
                                            'id="xs-components-links-module-UikitModule-154f798c77e8b15a35b3161d22cae498"' }>
                                            <li class="link">
                                                <a href="components/AccordionComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AccordionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AccordionGroupComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AccordionGroupComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AutoGridComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AutoGridComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BoxComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BoxComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ChartjsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ChartjsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ColComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ColComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GooglemapComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GooglemapComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GridComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GridComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SeparatorComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SeparatorComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SuperAutoGridComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SuperAutoGridComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/CounterService.html" data-type="entity-link">CounterService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});